#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/file_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

int capturedImg = 50;

pcl::visualization::PCLVisualizer v("DisplayPCL");
static const std::string OPENCV_WINDOW = "Image window";


void depthCallBack(const sensor_msgs::PointCloud2ConstPtr& input){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);


    pcl::fromROSMsg(*input, *cloud);
    cloud->width = (cloud->width * cloud->height);
    cloud->height = 1;

    if(capturedImg==50){
        pcl::VoxelGrid<pcl::PointXYZ> avg;
        avg.setInputCloud(cloud);
        avg.setLeafSize(0.005f, 0.005f, 0.005f);
        avg.filter(*cloud);

        std::cout << "is organized: " << cloud-> isOrganized() << std::endl;
        //pcl::io::savePCDFile("/home/kasper/catkin_ws/src/rovi2/pointclouds/pclWithFilter/LS=0,0025/smallObjectOffset.pcd", *cloud);
        std::cout << "Saved pcd file" << std::endl;
        capturedImg++;

    }

    //std::cout << "Called point CB" << std::endl;
    v.removePointCloud("object", 0);
    v.addPointCloud<pcl::PointXYZ>(cloud,pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>(cloud,0,255,0), "object");
    v.spinOnce(100);
}

void imageCallBack(const sensor_msgs::ImageConstPtr& image)
{
    cv_bridge::CvImagePtr cv_ptr;
    try{
        //For raw color images:     sensor_msgs::image_encodings::BGR8
        //For depth images:         sensor_msgs::image_encodings::TYPE_16UC1
        cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::TYPE_8UC1);
    }
    catch (cv_bridge::Exception& e){
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv::Mat eqDepth;
    //Equalize for better view
    cv::equalizeHist(cv_ptr->image,eqDepth);

    cv::Mat falseColorsMap;
    applyColorMap(eqDepth, eqDepth, cv::COLORMAP_BONE);

    cv::imshow(OPENCV_WINDOW, eqDepth); //regular 2d image
    cv::waitKey(3);

    // Output modified video stream
    //imagePub.publish(cv_ptr->toImageMsg());
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "rovi2");
    ros::NodeHandle nh;
    //ros::Subscriber imageSub = nh.subscribe("/camera/depth/image_rect_raw",1,&imageCallBack);
    ros::Subscriber sub = nh.subscribe("/camera/depth_registered/points", 1, &depthCallBack);

    ros::spin();
    return 0;
}
