#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/common/random.h>
#include <pcl/common/time.h>
#include <pcl/features/spin_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/spin_image.h>
#include <pcl/correspondence.h>
#include "pcl/kdtree/impl/kdtree_flann.hpp"
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/transformation_estimation.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

//For table segmentation
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/common/geometry.h>

//For cameraSubscriber And ROS
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/io.h>

//Publisher
#include <ros/ros.h>
#include <geometry_msgs/Transform.h>
#include <rw/math/Vector3D.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Quaternion.hpp>



//callback pcl
pcl::PointCloud<pcl::PointNormal>::Ptr callBackSceneCloud(new pcl::PointCloud<pcl::PointNormal>);

//ready for pose est bool
bool pcRdy4PoseEst = false;

void depthCallBack(const sensor_msgs::PointCloud2ConstPtr& input){
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud(new pcl::PointCloud<pcl::PointNormal>);

    pcl::fromROSMsg(*input, *cloud);
    cloud->width = (cloud->width * cloud->height);
    cloud->height = 1;

    pcl::VoxelGrid<pcl::PointNormal> avg;
    avg.setInputCloud(cloud);
    avg.setLeafSize(0.005f, 0.005f, 0.005f);
    avg.filter(*cloud);

    //std::cout << "is organized: " << cloud-> isOrganized() << std::endl;
    //pcl::io::savePCDFile("/home/kasper/catkin_ws/src/rovi2/pointclouds/pclWithFilter/LS=0,0025/smallObjectOffset.pcd", *cloud);
    //std::cout << "Saved pcd file" << std::endl;

    copyPointCloud(*cloud,*callBackSceneCloud);
    pcRdy4PoseEst = true;
}


void surfaceNormalEst(pcl::PointCloud<pcl::PointNormal>::Ptr scene, pcl::PointCloud<pcl::PointNormal>::Ptr object) {
    pcl::ScopeTime t("Surface normal estimation");
    pcl::NormalEstimation<pcl::PointNormal, pcl::PointNormal> normEst;
    normEst.setKSearch(10);
    //Or use normEst.setRadiusSearch(NUMBER)
    pcl::search::KdTree<pcl::PointNormal>::Ptr tree(new pcl::search::KdTree<pcl::PointNormal>());
    normEst.setSearchMethod(tree);

    normEst.setInputCloud(scene);
    normEst.compute(*scene);
    normEst.setInputCloud(object);
    normEst.compute(*object);

}

void spinFeatureExtraction(pcl::PointCloud<pcl::PointNormal>::Ptr scene, pcl::PointCloud<pcl::PointNormal>::Ptr object,
                           pcl::PointCloud<pcl::Histogram<153>>::Ptr sceneFeatures,
                           pcl::PointCloud<pcl::Histogram<153>>::Ptr objectFeatures) {

    pcl::ScopeTime t("Shape features");
    pcl::SpinImageEstimation<pcl::PointNormal, pcl::PointNormal, pcl::Histogram<153>> spin;

    //pcl::search::KdTree<pcl::PointNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointNormal> ());
    //spin.setSearchMethod(tree);

    spin.setRadiusSearch(0.05);

    //Calculate object features
    spin.setInputCloud(object);
    spin.setInputNormals(object);
    spin.compute(*objectFeatures);

    //Calculate scene features
    spin.setInputCloud(scene);
    spin.setInputNormals(scene);
    spin.compute(*sceneFeatures);

}
/*
void featureMatching(pcl::PointCloud<pcl::PointNormal>::Ptr scene,  pcl::PointCloud<pcl::PointNormal>::Ptr object, pcl::PointCloud<pcl::Histogram<153>>::Ptr sceneFeatures,  pcl::PointCloud<pcl::Histogram<153>>::Ptr objectFeatures){
    pcl::Correspondences corr(objectFeatures->size());

    // Use a KdTree to search for the nearest matches in feature space
    pcl::KdTreeFLANN<pcl::Histogram<153>> objKDTree;
    objKDTree.setInputCloud(sceneFeatures);

    // Find the index of the best match for each keypoint, and store it in "correspondences_out"
    std::vector<int> index(1);
    std::vector<float> sqDist(1);
    for (int i = 0; i < objectFeatures->size(); ++i){
        //objKDTree.radiusSearch(*objectFeatures,i,0.05,index,sqDist, 1);
        objKDTree.nearestKSearch(*objectFeatures,i,1,index,sqDist);
        corr[i].index_query = i;
        corr[i].index_match = index[0];
    }

    std::cout << "objCloud size:" << objectFeatures->size() << std::endl;
    std::cout << "SceneCloud size:" << sceneFeatures->size() << std::endl;
    pcl::visualization::PCLVisualizer v("Matches");
    v.addPointCloud<pcl::PointNormal>(object, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(object, 0, 255, 0), "object");
    v.addPointCloud<pcl::PointNormal>(scene, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(scene, 255, 0, 0),"scene");
    v.addCorrespondences<pcl::PointNormal>(object, scene, corr, 1);
    v.spin();
}

*/


pcl::Correspondences
featureMatching(const pcl::PointCloud<pcl::PointNormal>::Ptr scene, const pcl::PointCloud<pcl::PointNormal>::Ptr object,
                pcl::PointCloud<pcl::Histogram<153>>::Ptr sceneFeatures,
                pcl::PointCloud<pcl::Histogram<153>>::Ptr objectFeatures) {
    pcl::ScopeTime t("feature matching");
    // Find the index of the best match for each keypoint, and store it in "correspondences_out"

    pcl::Correspondences corr(objectFeatures->size());

    float minDist;
    float tmpDist = 0.0;
    int matchIndex = 0;
    float diff = 0;
    for (int i = 0; i < objectFeatures->size(); i++) {
        minDist = INFINITY;
        for (int j = 0; j < sceneFeatures->size(); j++) {
            tmpDist = 0.0;
            //Calculate distance between features
            for (int k = 0; k < pcl::Histogram<153>::descriptorSize(); k++) {
                diff = reinterpret_cast<const float *>(&sceneFeatures->points[j])[k] -
                       reinterpret_cast<const float *>(&objectFeatures->points[i])[k];
                tmpDist += diff * diff;
            }
            //save index if it is the closest feature
            if (tmpDist < minDist) {
                minDist = tmpDist;
                matchIndex = j;
            }
        }

        //std::cout << "correspondance " << i << " | match index in scene: "<< matchIndex << " | minDist: " << minDist << std::endl;
        corr[i].index_query = i;
        corr[i].index_match = matchIndex;
    }

    std::cout << "objCloud size:" << objectFeatures->size() << std::endl;
    std::cout << "SceneCloud size:" << sceneFeatures->size() << std::endl;
    pcl::visualization::PCLVisualizer v("Matches");
    v.addPointCloud<pcl::PointNormal>(object, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(object, 0, 255, 0), "object");
    v.addPointCloud<pcl::PointNormal>(scene, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(scene, 255, 255, 255),"scene");
    v.addCorrespondences<pcl::PointNormal>(object, scene, corr, 1);
    v.spin();

    return corr;
}

void displayPCL(const pcl::PointCloud<pcl::PointNormal>::Ptr object, const pcl::PointCloud<pcl::PointNormal>::Ptr scene, std::string name) {

    pcl::visualization::PCLVisualizer v(name);
    v.addPointCloud<pcl::PointNormal>(object,pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(object, 0, 255,0), "object");
    v.addPointCloud<pcl::PointNormal>(scene,pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(scene, 255, 255,255), "scene");
    v.spin();
}


Eigen::Matrix4f ransac(int iterations, float inlierThresh, pcl::Correspondences corr,
            const pcl::PointCloud<pcl::PointNormal>::Ptr scene, const pcl::PointCloud<pcl::PointNormal>::Ptr object) {
    //Run for x itrations
    pcl::ScopeTime t("RANSAC");

    std::cout << "corr size: " << corr.size() << std::endl;
    std::cout << "object size: " << object->size() << std::endl;
    std::cout << "scene size: " << scene->size() << std::endl;

    int bestInlierCount = 0;
    float bestRMSE = 0;
    pcl::PointCloud<pcl::PointNormal>::Ptr bestTransformPCL(new pcl::PointCloud<pcl::PointNormal>);
    Eigen::Matrix4f bestTransform;
    // Use a KdTree to search for the nearest matches in feature space
    pcl::search::KdTree<pcl::PointNormal> sceneKDTree;
    sceneKDTree.setInputCloud(scene);

    pcl::PointCloud<pcl::PointNormal>::Ptr transformedPCL(new pcl::PointCloud<pcl::PointNormal>);

    std::vector<std::vector<int>> nearestIndex;
    std::vector<std::vector<float>> nearestDist;
    srand((int)time(0));
    for (int i = 0; i < iterations; i++) {
        //Generate random x samples pairs from correlation
        //std::cout << "Iteration: " << i <<  std::endl;

        std::vector<int> indexQuery(3);
        std::vector<int> indexMatch(3);
        for (int j = 0; j < 3; j++) {
            const int randIndex = std::rand() % (corr.size());
            indexQuery[j] = (corr[randIndex].index_query);
            indexMatch[j] = (corr[randIndex].index_match);
        }

        //Estimate transform
        pcl::registration::TransformationEstimationSVD<pcl::PointNormal, pcl::PointNormal> transEst;
        Eigen::Matrix4f transformMat;

        transEst.estimateRigidTransformation(*object, indexQuery, *scene, indexMatch, transformMat);

        pcl::transformPointCloud(*object, *transformedPCL, transformMat);

        sceneKDTree.nearestKSearch(*transformedPCL, std::vector<int>(), 1, nearestIndex, nearestDist);

        //Compute inliers
        int inlierCount = 0;
        float rmsError = 0;
        for (size_t k = 0; k < nearestDist.size(); k++) {
            //std::cout << "Distance: " << nearestDist[k][0] << std::endl;
            if (nearestDist[k][0] < inlierThresh) {
                inlierCount++;
                rmsError += (nearestDist[k][0] * nearestDist[k][0]);
            }
        }
        rmsError = sqrt(rmsError / nearestDist.size());

        //float successProb = pow((1-std::pow(inlierRatio,inlierCount)),iterations);

        //if score is less than earlier results then replace with new
        if (inlierCount > bestInlierCount) {
            bestInlierCount = inlierCount;
            bestTransform = transformMat;
            bestRMSE = rmsError;
            std::cout << "Iteration:" << i << " | RMS Error: " << bestRMSE << " | Inlier Count: " << bestInlierCount << std::endl;
        }
    }

    pcl::transformPointCloud(*object, *transformedPCL, bestTransform);

    //displayPCL(transformedPCL, scene, "alligned");

    std::cout << "Transform: " << std::endl << bestTransform << std::endl;

    return bestTransform;
}


void planeSeg(pcl::PointCloud<pcl::PointNormal>::Ptr scene, pcl::PointCloud<pcl::PointNormal>::Ptr sceneOutput,pcl::PointCloud<pcl::PointNormal>::Ptr filteredScene, double inlierThresh, double outlierThresh){
    pcl::ScopeTime t("Plane Segmentation");
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

    /*
     //One time run to estimate the model coef
     //http://www.pointclouds.org/documentation/tutorials/planar_segmentation.php#id1
     // Create the segmentation object
     pcl::SACSegmentation<pcl::PointNormal> seg;
     // Optional
     seg.setOptimizeCoefficients (true);
     // Mandatory
     seg.setModelType (pcl::SACMODEL_PLANE);
     seg.setMethodType (pcl::SAC_RANSAC);
     seg.setDistanceThreshold (0.01);

     seg.setInputCloud (scene);
     seg.segment (*inliers, *coefficients);
 */

    //Model coeffs created from plane without any objects( Model coefficients: -0.0134264 0.508509 0.860952 -0.599346)
    std::vector<float> coefVec = {-0.0134264,0.508509,0.860952,-0.599346};
    coefficients->values = coefVec;

    //std::cerr << "Model coefficients: " << coefficients->values[0] << " "<< coefficients->values[1] << " " << coefficients->values[2] << " " << coefficients->values[3] << std::endl;

    // Create the filtering object
    pcl::ProjectInliers<pcl::PointNormal> proj;
    proj.setModelType (pcl::SACMODEL_PLANE);
    proj.setInputCloud (scene);
    proj.setModelCoefficients (coefficients);
    proj.filter(*sceneOutput);

    double dist = 0;
    int j = 0;

    filteredScene->width  = sceneOutput->width;
    filteredScene->height = 1;
    filteredScene->points.resize (filteredScene->width * filteredScene->height);

    for(std::size_t i = 0; i < sceneOutput->points.size(); i++){
        dist = pcl::geometry::distance(sceneOutput->points[i],scene->points[i]);

        if( dist > inlierThresh && dist < outlierThresh){
            filteredScene->points[j] = scene->points[i];
            j++;
        }
    }
}

//WRITTEN BY ANDERS
Eigen::Matrix4f ICP(pcl::PointCloud<pcl::PointNormal>::Ptr scene_cloud,
                                           pcl::PointCloud<pcl::PointNormal>::Ptr object_local_cloud,
                                           Eigen::Matrix4f ransacTransform,
                                           bool visualize)
{
    //Transform object pointcloud to the ransac pose
    pcl::transformPointCloud(*object_local_cloud, *object_local_cloud, ransacTransform);

    // ICP
    pcl::search::KdTree<pcl::PointNormal> kdtree; // KD TREE
    kdtree.setInputCloud(scene_cloud);
    const size_t iter = 50; // NUMBER OF ITERATIONS
    const float thressq = 0.01*0.01; // THRESHHOLD

    Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
    pcl::PointCloud<pcl::PointNormal>::Ptr ICP_cloud(new pcl::PointCloud<pcl::PointNormal>(*object_local_cloud));
    pcl::ScopeTime t("ICP");

    //Make pose vector
    Eigen::Matrix4f poseArray[iter] = {};

    for(size_t i = 0; i < iter; i++) {
        // 1) Find closest points
        std::vector<std::vector<int> > idx;
        std::vector<std::vector<float> > distsq;

        kdtree.nearestKSearch(*ICP_cloud, std::vector<int>(), 1, idx, distsq);

        // Threshold and create indices for object/scene and compute RMSE
        std::vector<int> idxobj;
        std::vector<int> idxscn;
        for (size_t j = 0; j < idx.size(); j++) {
            if (distsq[j][0] <= thressq) {
                idxobj.push_back(j);
                idxscn.push_back(idx[j][0]);
            }
        }

        // 2) Estimate transformation
        Eigen::Matrix4f Transform;
        pcl::registration::TransformationEstimationSVD<pcl::PointNormal, pcl::PointNormal> est;
        est.estimateRigidTransformation(*ICP_cloud, idxobj, *scene_cloud, idxscn, Transform);

        // 3) Apply pose
        transformPointCloud(*ICP_cloud, *ICP_cloud, Transform);

        // 4) Update result
        pose = Transform * pose;
        poseArray[i] = pose;
    }
    // Compute inliers and RMSE
    std::vector<std::vector<int> > idx;
    std::vector<std::vector<float> > distsq;
    kdtree.nearestKSearch(*ICP_cloud, std::vector<int>(), 1, idx, distsq);
    size_t inliers = 0;
    float rmse = 0;
    for(size_t i = 0; i < distsq.size(); i++)
        if(distsq[i][0] <= thressq)
            ++inliers, rmse += distsq[i][0];
    rmse = sqrtf(rmse / inliers);

    Eigen::Matrix4f camTAllingedObject = ransacTransform;

    for(int i = 0; i < iter; i++){
        camTAllingedObject = ransacTransform*poseArray[i];
    }


    if (visualize){
        std::cout << "Pose:" << endl << camTAllingedObject << std::endl;
        std::cout << "Inliers: " << inliers << "/" << object_local_cloud->size() << std::endl;
        std::cout << "RMSE: " << rmse << std::endl;
        pcl::transformPointCloud(*ICP_cloud, *ICP_cloud, pose);
        pcl::visualization::PCLVisualizer viewer("After ICP alignment");
        viewer.addPointCloud<pcl::PointNormal>(ICP_cloud, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(ICP_cloud, 0, 255, 0), "object_aligned");
        viewer.addPointCloud<pcl::PointNormal>(scene_cloud, pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal>(scene_cloud, 255, 0, 0),"scene");
        viewer.spin();
    }

    return camTAllingedObject;
}

geometry_msgs::Transform eigenToTransform(Eigen::Matrix4f camTObject){
    rw::math::Rotation3D<float> rotation(   camTObject(0,0), camTObject(0,1), camTObject(0,2),
                                            camTObject(1,0), camTObject(1,1), camTObject(1,2),
                                            camTObject(2,0), camTObject(2,1), camTObject(2,2));
    rw::math::Quaternion<float> quaternion(rotation);

    geometry_msgs::Vector3 geoVec;
    geoVec.x = camTObject(0,3);
    geoVec.y = camTObject(1,3);
    geoVec.z = camTObject(2,3);

    geometry_msgs::Quaternion geoQuaternion;
    geoQuaternion.x = quaternion.getQx();
    geoQuaternion.y = quaternion.getQy();
    geoQuaternion.z = quaternion.getQz();
    geoQuaternion.w = quaternion.getQw();

    geometry_msgs::Transform camTObjectMsg;
    camTObjectMsg.rotation = geoQuaternion;
    camTObjectMsg.translation = geoVec;

    return camTObjectMsg;

}


Eigen::Matrix4f poseExtraction(pcl::PointCloud<pcl::PointNormal>::Ptr scene, pcl::PointCloud<pcl::PointNormal>::Ptr object, int iterations) {
    //Compute surface normals(To be able to use spin)
    pcl::ScopeTime t("Total pose estimation");
/*
    surfaceNormalEst(scene, object);

    //Compute features(With spin)
    pcl::PointCloud<pcl::Histogram<153>>::Ptr objectFeatures(new pcl::PointCloud<pcl::Histogram<153>>);
    pcl::PointCloud<pcl::Histogram<153>>::Ptr sceneFeatures(new pcl::PointCloud<pcl::Histogram<153>>);

    spinFeatureExtraction(scene, object, sceneFeatures, objectFeatures);

    pcl::Correspondences corr = featureMatching(scene, object, sceneFeatures, objectFeatures);

    Eigen::Matrix4f ransacTransform = ransac(iterations, 0.0001, corr, scene, object);
    */

    //Transform works for bagfile firstposition_camdepth.bag
    Eigen::Matrix4f ransacTransform;
    ransacTransform << -0.980004, -0.12772,-0.152572,0.219988,-0.196791, 0.508871, 0.838048,0.143305, -0.0293962,0.851315, -0.52383, 0.600836, 0,0,0,1;

    return ICP(scene,object,ransacTransform, false);
}

int main(int argc, char *argv[]) {
    //Start Cam Subscriber node

    //Removes warnings when loading pointcloud into normalVector cloud
    pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);

    ros::init(argc, argv, "PoseEstimation");
    ros::NodeHandle nh;
    //ros::Subscriber imageSub = nh.subscribe("/camera/depth/image_rect_raw",1,&imageCallBack);
    ros::Subscriber sub = nh.subscribe("/camera/depth_registered/points", 1, &depthCallBack);
    ros::Publisher pub = nh.advertise<geometry_msgs::Transform>("rovi2/camTobject", 1);

    pcl::PointCloud<pcl::PointNormal>::Ptr SGSGlobal(new pcl::PointCloud<pcl::PointNormal>);
    pcl::PointCloud<pcl::PointNormal>::Ptr sceneCloud(new pcl::PointCloud<pcl::PointNormal>);


    ros::Rate loop_rate(10);
    while(ros::ok()){
        if(pcRdy4PoseEst){
            //LOAD OBJECT FROM FILE
            if (pcl::io::loadPCDFile<pcl::PointNormal>(argv[1], *SGSGlobal) == -1) //* load the OBJECT file
            {
                PCL_ERROR ("Failed to load PointCloud \n");
                return (-1);
            }

            //LOAD POINT CLOUD FROM CALLBACK
            copyPointCloud(*callBackSceneCloud,*sceneCloud);

            //Show before GlobalAllignment
            //displayPCL(SGSGlobal, sceneCloud, "Initial scene and object");

            //Perform plane segmentation and remove the plane
            pcl::PointCloud<pcl::PointNormal>::Ptr sceneCloudProjected(new pcl::PointCloud<pcl::PointNormal>);
            pcl::PointCloud<pcl::PointNormal>::Ptr sceneCloudFiltered(new pcl::PointCloud<pcl::PointNormal>);
            planeSeg(sceneCloud, sceneCloudProjected,sceneCloudFiltered, 0.01, 0.1);

            //Show after pre-processing
            //displayPCL(sceneCloudFiltered, SGSGlobal, "FilteredCloud");

            //Perform pose extraction
            Eigen::Matrix4f camTObject = poseExtraction(sceneCloudFiltered, SGSGlobal, 10000);

            pub.publish(eigenToTransform(camTObject));

            pcRdy4PoseEst = false;
        }
        ros::spinOnce();
        loop_rate.sleep();
    }
    return (0);
}