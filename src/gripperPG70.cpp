#include "rw/rw.hpp"
#include "caros/gripper_si_proxy.h"
#include "ros/package.h"
#include <iostream>
#include <rw/kinematics.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/proximity.hpp>
#include <rw/math/Transform3D.hpp>
#include <caros_control_msgs/GripperState.h>
#include <std_msgs/Float64.h>

rw::math::Q gripperState;
double callbackConf;

class GripperPG70 {
    using Q = rw::math::Q;


private:
    ros::NodeHandle nh;
    rw::kinematics::State state;
    caros::GripperSIProxy* gripper;

public:
    GripperPG70(){
        //Change this to subscribe to robot control module instead

        auto packagePath = ros::package::getPath("rovi2");
        gripper = new caros::GripperSIProxy(nh, "caros_schunkpg70");
        // Wait for first state message, to make sure robot is ready
        ros::topic::waitForMessage<caros_control_msgs::GripperState>("/caros_schunkpg70/caros_gripper_service_interface/gripper_state", nh);
        ros::spinOnce();

    }

    bool moveQ(Q q){
        return gripper->moveQ(q);
    }

    bool gripQ(Q q){
        //set the desired force configuration that the gripper should use.
        return gripQ(q);
    }

    Q getQ(){
        return gripper->getQ();
    }

    Q getQd(){
        return gripper->getQd();
    }

    Q getForce(){
        return gripper->getForce();
    }


    bool stopGripper(){
        return gripper->stopMovement();
    }

    rw::kinematics::State getState(){
        return state;
    }

    bool setForce(Q q){
        return gripper->setForceQ(q);
    }

};

void gripperStateCallback(const caros_control_msgs::GripperState::ConstPtr &state) {
    std::cout << "Gripper State:" << std::endl;
    gripperState = state->q.data;
}

void gripperConfCallback(const std_msgs::Float64 conf){
    callbackConf = conf.data;

    std::cout << callbackConf << std::endl;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "gripperPG70");
    ros::NodeHandle n("~");
    ros::Subscriber stateSub = n.subscribe("/caros_schunkpg70/caros_serial_device_service_interface/gripper_state", 10, gripperStateCallback);
    ros::Subscriber controlSub = n.subscribe("/rovi2/gripperMsg", 10, gripperStateCallback);

    ros::spin();

    GripperPG70 pg70;


    //Max config 0.03
    rw::math::Q q(1,0.034);
    rw::math::Q q1(1,0.000);
    rw::math::Q forceConfig(1,0.1);

    pg70.setForce(forceConfig);


    if(pg70.moveQ(q))
        std::cout << "New joint config: " << pg70.getQ() << "Current gripping force: "  << pg70.getForce() << std::endl;
    else
        std::cout << "failed to move to config: " << q << " Current config: "<< pg70.getQ()<< std::endl ;


    if(pg70.moveQ(q1))
        std::cout << "New joint config: " << pg70.getQ() << "Current gripping force: "  << pg70.getForce() << std::endl;
    else
        std::cout << "failed to move to config: " << q1 << std::endl;


    std::cout << "jimmi" << std::endl;

    std::cout <<"Force: " << pg70.getForce() << std::endl;

    pg70.stopGripper();



	return 0;
}
