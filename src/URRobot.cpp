#include "rw/rw.hpp"
#include "caros/serial_device_si_proxy.h"
#include "ros/package.h"
#include <iostream>
#include <fstream>
#include <rw/kinematics.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/proximity.hpp>
#include <rw/math/Transform3D.hpp>
#include <rwlibs/algorithms/PointPairsRegistration.hpp>
#include <rw/math/EAA.hpp>
#include <rw/math/Quaternion.hpp>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <rw/invkin/JacobianIKSolver.hpp>
#include <rw/invkin/InvKinSolver.hpp>
#include <geometry_msgs/Transform.h>
#include <std_msgs/Float64.h>


//Collision detection
#include <rw/proximity/CollisionDetector.hpp>
#include <rw/proximity/ProximityStrategy.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>

//Includes for file reading
#include <fstream>
#include <sstream>
#include <string>


//FOR CALLBACK AND COMUNICATION
bool newDataAvailable = true;
rw::math::Transform3D<double> cbTransform;
//baseTcam Transform (Estimated by jimmi group)

rw::math::Rotation3D<double> baseTcamRos(   -0.0156, 0.9994, 0.0303,
                                            -0.5083, 0.0182, -0.8610,
                                            -0.8611, -0.0288, 0.5077);

rw::math::Vector3D<double> bateTcamDisp(0.3834,0.8302,0.6043);
rw::math::Transform3D<double> baseTcam(bateTcamDisp, baseTcamRos);

//AR-TRACKER
rw::math::Vector3D<> markerPointsTbase;
rw::math::Vector3D<> camPointsTmarker;
std::vector<rw::math::Vector3D<>> camPointsTmarkerVec;
std::vector<rw::math::Vector3D<>> markerPointsTbaseVec;
typedef std::pair< rw::math::Vector3D<>, rw::math::Vector3D<>> 	PointPair;
std::ofstream markerTransFile ("markerTransfile");
rw::math::Transform3D<> cameraTbase;
rw::math::Transform3D<> markerTcamera;
rw::math::Transform3D<> markerTbase;
std::vector<rw::math::Transform3D<>> cameraTbaseVec;
rw::math::Vector3D<> eaaComplete;
rw::math::Vector3D<> posComplete;

//Globals for Collisionchecking
double epsilon = 0.01;
int collisionCheck = 0;

class URRobot {
	using Q = rw::math::Q;


private:
	ros::NodeHandle nh;
	rw::models::WorkCell::Ptr wc;
	rw::models::Device::Ptr device;
	rw::kinematics::State state;
	caros::SerialDeviceSIProxy* robot;
    rw::proximity::CollisionDetector::Ptr detector;


public:
	URRobot()
	{
		auto packagePath = ros::package::getPath("rovi2");
		wc = rw::loaders::WorkCellLoader::Factory::load(packagePath + "/WorkCell/Scene.wc.xml");
		device = wc->findDevice("UR5");
		state = wc->getDefaultState();
		robot = new caros::SerialDeviceSIProxy(nh, "caros_universalrobot");
        detector = new rw::proximity::CollisionDetector(wc, rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy());
		// Wait for first state message, to make sure robot is ready
		ros::topic::waitForMessage<caros_control_msgs::RobotState>("/caros_universalrobot/caros_serial_device_service_interface/robot_state", nh);
	    ros::spinOnce();
	}

	Q getQ()
	{
		// spinOnce processes one batch of messages, calling all the callbacks
	    ros::spinOnce();
	    Q q = robot->getQ();
		device->setQ(q, state);
	    return q;
	}

    bool setQ(Q q, caros::SerialDeviceSIProxy* dev)
    {
        // Tell robot to move to joint config q
        float speed = 0.2;
        if (dev->movePtp(q, speed)) {
            return true;
        } else
            return false;
    }


    bool setQ(Q q)
	{
		// Tell robot to move to joint config q
        float speed = 0.2;
		if (robot->movePtp(q, speed)) {
		    device->setQ(q,state);
			return true;
		} else
			return false;
	}

	bool moveQ(Q q){
        return setQ(robot->getQ() + q);
	}

	bool moveHome(){
        Q defaultQ = device->getQ(wc->getDefaultState());
        return setQ(defaultQ);
	}

	rw::math::Transform3D<> getPose(){
	    return device->getEnd()->getTransform(state);
	}


    rw::kinematics::State getState(){
	    return state;
	}

    rw::models::Device::Ptr getDevice(){
	    return device;
	}

	rw::math::Transform3D<double> getbaseTend(){
	    return getDevice()->baseTend(state);
	}

    std::vector<rw::math::Q> inverseKin(rw::math::Transform3D<> transform){
        rw::invkin::JacobianIKSolver invK(getDevice(),getState());
        invK.setMaxError(0.00001);
        invK.setMaxIterations(1000);
        return invK.solve(transform, getState());
    }


	//TAKEN FROM BasicCollision.cpp FROM collisionTest_Result ROVI2 BlackBoard
    bool inCollision(Q q){
        device->setQ(q, state);
        rw::proximity::CollisionDetector::QueryResult data;
        collisionCheck++;
        bool collision = detector->inCollision(state,&data);
        return collision;
    }

    bool binarySearch(Q q1, Q q2){
        Q dq = q2-q1;
        double N = dq.norm2()/epsilon;
        int levels = ceil(log2(N));
        Q qi;
        for (int i = 1; i <= levels; i++){
            int steps = pow(2, i-1);
            Q step = dq/steps;
            for (int k = 1; k <= steps; k++){
                qi = q1+((double)k-0.5)*step;
                if (inCollision(qi)){
                    return false;
                }
            }
        }
        return true;
    }

};

void transformCallback(geometry_msgs::TransformPtr transform){
    rw::math::Vector3D<double> translation = rw::math::Vector3D<double>(transform->translation.x,transform->translation.y,transform->translation.z);
    rw::math::Quaternion<double> rotationQuaternion = rw::math::Quaternion<double>(transform->rotation.x,transform->rotation.y,transform->rotation.z,transform->rotation.w);
    cbTransform = rw::math::Transform3D<double>(translation,rotationQuaternion);
    std::cout << "cbTransform: " << cbTransform << std::endl;
    newDataAvailable = 1;
}

void arCallback(ar_track_alvar_msgs::AlvarMarkersPtr input){
    std::cout << "marker queue size: " << input->markers.size() << std::endl;
    if(input->markers.size() == 9) {
        double avgX = 0;
        double avgY = 0;
        double avgZ = 0;

        for (const auto &marker : input->markers) {
            avgX += marker.pose.pose.position.x;
            avgY += marker.pose.pose.position.y;
            avgZ += marker.pose.pose.position.z;
        }
        avgX = avgX/input->markers.size();
        avgY = avgY/input->markers.size();
        avgZ = avgZ/input->markers.size();

        rw::math::Quaternion<double> quarternion(   input->markers[0].pose.pose.orientation.x,
                                                    input->markers[0].pose.pose.orientation.y,
                                                    input->markers[0].pose.pose.orientation.z,
                                                    input->markers[0].pose.pose.orientation.w);

        rw::math::Vector3D<> pos(avgX, avgY, avgZ);


        rw::math::Rotation3D<> rot = quarternion.toRotation3D();
        rw::math::Transform3D<> markerTrans(pos, rot);

        markerTcamera = markerTrans;

/*
        markerTransFile.open ("markerTransfile.txt");

        markerTransFile << markerTrans.P() << "," << markerTrans.R() << "\n";

        markerTransFile.close();
*/

    }
}

//Call this function to run the callibration
void arTracker(){
    ros::NodeHandle n("~");

    ros::Subscriber sub = n.subscribe<ar_track_alvar_msgs::AlvarMarkersPtr> ("/ar_pose_marker", 1, &arCallback);


    URRobot robot;
    std::cout << "Current joint config:" << std::endl << robot.getQ() << std::endl << std::endl;
    std::cout << "Input destination joint config in radians:" << std::endl;

    //Define Q's for caliration
    rw::math::Q q1(6, 0.7732124328613281, -1.0053818982890625, 2.140766445790426, -2.1643673382201136, 1.160172939300537, -1.7588465849505823);
    rw::math::Q q2(6, 0.5958089828491211, -0.5720837873271485, 1.3198588530169886, -2.1279221973814906, 0.7736911773681641, -2.633404795323507);
    rw::math::Q q3(6, 0.8070425987243652, -0.9067686361125489, 1.3945773283587855, -1.5328548711589356, 0.9157900810241699, -3.199463669453756);
    rw::math::Q q4(6, 0.7325644493103027, -0.9267538350871583, 1.7901080290423792, -2.11643185238027, 1.0915303230285645, -4.623188320790426);

    std::vector<rw::math::Q> qs{q1,q2,q3,q4};

    while (ros::ok())
    {

//Transformation based on averages

        for(const rw::math::Q& i : qs)
        {
            if(robot.setQ(i))
            {
                boost::this_thread::sleep_for (boost::chrono::seconds(3));

                std::cout << std::endl << "New joint config:" << std::endl << robot.getQ() << std::endl;
                markerTbase = robot.getPose();
                cameraTbase = markerTbase * inverse(markerTcamera);
                std::cout << "MARKER T CAMERA: " << markerTcamera << std::endl;
                std::cout << "CAMERA T BASE: " << cameraTbase << std::endl;
                cameraTbaseVec.push_back(cameraTbase);
                std::cout << std::endl << "Cam to base size: " << cameraTbaseVec.size() << std::endl;

            }else{std::cout << std::endl << "Failed to move robot" << std::endl;}

            boost::this_thread::sleep_for (boost::chrono::seconds(3));
        }


        for (int i; i < cameraTbaseVec.size(); i++) {
            rw::math::Rotation3D<> cameraTbaseRot = cameraTbaseVec[i].R();
            std::cout << "CAMERA T BASE ROT: " << cameraTbaseRot << std::endl;
            rw::math::EAA<> eaaRobot(cameraTbaseRot);
            std::cout << "EAA ROBOT: " << eaaRobot << std::endl;
            eaaComplete += (eaaRobot.angle() * eaaRobot.axis());
            std::cout << "EAA COMPLETE: " << eaaComplete << std::endl;
            posComplete += cameraTbaseVec[i].P();
            std::cout << "POS COMPLETE: " << posComplete << std::endl;
        }

        std::cout<< std::endl << "eaaComplete: " << eaaComplete << std::endl;
        std::cout << "posComplete: " <<  posComplete << std::endl;
        std::cout << std::endl << "camTbaseVec: " <<  cameraTbaseVec.size() << std::endl;

        eaaComplete = eaaComplete / cameraTbaseVec.size();
        posComplete = posComplete / cameraTbaseVec.size();

        std::cout << "EAA: " << eaaComplete << std::endl;
        rw::math::EAA<> vecToEAA(eaaComplete);
        std::cout << "Vec to EAA: " << vecToEAA << std::endl;
        rw::math::Rotation3D<> eaaToRotation(vecToEAA.toRotation3D());
        std::cout << "EAA to rotation: " << eaaToRotation << std::endl;
        std::cout << "Pos: " << posComplete << std::endl;

        rw::math::Transform3D<> camTbaseComplete(posComplete, eaaToRotation);
        std::cout << std::endl << "CAM TO BASE COMPLETE TRANSFORM: " << camTbaseComplete << std::endl;



//Transformation based on SVD

/*

    for(const rw::math::Q& i : qs)
    {
        if(robot.setQ(i))
        {
            std::cout << std::endl << "New joint config:" << std::endl << robot.getQ() << std::endl;
            markerTbase = robot.getPose();

            boost::this_thread::sleep_for (boost::chrono::seconds(3));

            camPointsTmarker += markerTcamera.P();
            markerPointsTbase += markerTbase.P();

            camPointsTmarkerVec.push_back(markerTcamera.P());
            markerPointsTbaseVec.push_back(markerTbase.P());
            std::cout << "MARKER T CAMERA: " << markerTcamera << std::endl;
            std::cout << "MARKER T BASE: " << markerTbase << std::endl;
            std::cout << "CAM POINTS T MARKER VEC SIZE: " << camPointsTmarkerVec.size() << std::endl;

        }else{std::cout << std::endl << "Failed to move robot" << std::endl;}

        boost::this_thread::sleep_for (boost::chrono::seconds(3));
    }

    rw::math::Vector3D<> camCentroid(camPointsTmarker/qs.size());
    rw::math::Vector3D<> baseCentroid(markerPointsTbase/qs.size());

    for (int i; i = camPointsTmarkerVec.size(); i++)
    {
        rw::math::Vector3D<> camTransposed((camPointsTmarkerVec[i]-camCentroid));

        Eigen::Matrix<float,3,1> H((markerPointsTbaseVec[i]-baseCentroid).e() * camTransposed.e().transpose());
    }


*/


        //ros::Rate r(50); // 50 hz
        //while (ros::ok())
        //{
        //// TODO: If a new point has been added to the markerPoint Queue, Get a corresponding base point from URRobot

        //// TODO: calculate a new Rotation with SVD/average of EAA(If SVD is time consuming make the looprate slower)

        //// TODO: Move the ROBOT while fetching points from base and marker

        ros::spinOnce();
        //r.sleep();
    }

}

void tester(){

    //INIT ROBOT
    URRobot robot;

    //Define Q's for caliration
    rw::math::Q q1(6, 0.7732124328613281, -1.0053818982890625, 2.140766445790426, -2.1643673382201136, 1.160172939300537, -1.7588465849505823);
    rw::math::Q q2(6, 0.5958089828491211, -0.5720837873271485, 1.3198588530169886, -2.1279221973814906, 0.7736911773681641, -2.633404795323507);
    rw::math::Q q3(6, 0.8070425987243652, -0.9067686361125489, 1.3945773283587855, -1.5328548711589356, 0.9157900810241699, -3.199463669453756);
    rw::math::Q q4(6, 0.7325644493103027, -0.9267538350871583, 1.7901080290423792, -2.11643185238027, 1.0915303230285645, -4.623188320790426);

    std::vector<rw::math::Q> qs{q1,q2,q3,q4};

    //Call this function to run the callibration
    //arTracker();

    rw::math::Q q(6, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1);

    rw::math::Transform3D<double> trans;

    if(robot.moveHome()){
        std::cout << std::endl << "Successfully moved robot" << std::endl;
    }else{std::cout << std::endl << "Failed to move robot" << std::endl;}

    rw::math::Q initialQ = robot.getQ();

    if(robot.moveQ(q)){
        std::cout << std::endl << "Successfully moved robot" << std::endl;
    }else{std::cout << std::endl << "Failed to move robot" << std::endl;}


    trans = robot.getDevice()->baseTend(robot.getState());
    std::cout << "Transform" << trans << std::endl;

    std::vector<rw::math::Q> qInv = robot.inverseKin(trans);
    std::cout << "q Size: " << qInv.size() << std::endl;
    std::cout << "Initial q: " << initialQ << std::endl;
    std::cout << "Inv kin q: " << qInv[0] << std::endl;

    if(!robot.binarySearch(initialQ,qInv[0])){
        std::cout << "COLLISION DETECTION FAILED" << std::endl;
    }else{std::cout << "NO COLLISIONS DETECTED" << std::endl;}

    std::cout << "CollisionChecks: " << collisionCheck << std::endl;

    if(robot.moveHome()){
        std::cout << std::endl << "Successfully moved robot" << std::endl;
    }else{std::cout << std::endl << "Failed to move robot" << std::endl;}

    if(robot.setQ(qInv[0])){
        std::cout << std::endl << "Successfully moved robot" << std::endl;
    }else{std::cout << std::endl << "Failed to move robot" << std::endl;}

}

std::vector<rw::math::Transform3D<double>> readTransformFromFile(std::string path){
    std::ifstream infile(path);
    std::string line;
    std::cout << "Reading From file..." << std::endl;
    rw::math::Transform3D<double> tempTrans;
    std::vector<rw::math::Transform3D<double>> transformVec;

    if (!infile.is_open())
        perror("error while opening file");
    while(getline(infile, line)) {
        rw::math::Vector3D<double> lineVec;
        rw::math::Rotation3D<double> lineRot;
        int i = 0;
        std::stringstream ss(line);
        while( ss.good() )
        {
            std::string substr;
            getline( ss, substr, ',' );
            if(i < 3){
                lineVec(i) = stod(substr);
            }else if(i < 12){
                int row = (i-3)/3;
                int col =(i-3);
                if(col > 2){
                    if(col > 5){
                        col = col-3;
                    }
                    col = col-3;
                }
                lineRot(row,col) = stod(substr);
            }
            i++;
        }
        tempTrans = rw::math::Transform3D<double>(lineVec,lineRot);
        transformVec.push_back(tempTrans);
    }
    if (infile.bad())
        perror("error while reading file");
    std::cout << "Successfully read file with " << transformVec.size()  << " Transforms." << std::endl;
    return(transformVec);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "URRobot");
	ros::NodeHandle n("~");

    //INIT ROBOT
    URRobot robot;

    ros::Subscriber sub = n.subscribe("/rovi2/camTobject", 1, &transformCallback);

    //Gripper Publisher
    ros::Publisher pub = n.advertise<std_msgs::Float64>("rovi2/gripperMsg", 1);
    //readTransformFromFile(argv[1]);
    std_msgs::Float64 gripperConf;
    gripperConf.data = 0.03;


    rw::math::Vector3D<double> translation = rw::math::Vector3D<double>(-0.000799005, -0.284, 0.99963);
    rw::math::Rotation3D<double> rotation = rw::math::Rotation3D<double>(3.64677e-15, 0.999999, -0.00159255, -1, 3.64666e-15, -7.01612e-17, -6.43537e-17, 0.00159255, 0.999999);
    rw::math::Transform3D<double> trans(translation,rotation);
    ros::Rate loop_rate(10);
    while(ros::ok()) {
        if(newDataAvailable){
            if(robot.moveHome()){
                std::cout << std::endl << "Successfully moved robot" << std::endl;
            }else{std::cout << std::endl << "Failed to move robot" << std::endl;}
            std::cout << robot.inverseKin(trans)[0] << std::endl;

            pub.publish(gripperConf);

            newDataAvailable = false;
        }
    ros::spinOnce();
    loop_rate.sleep();
    }

    ros::spinOnce();

    return 0;
}
