#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/file_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/point_cloud.h>
#include <pcl/common/random.h>
#include <pcl/common/time.h>
#include <pcl/features/spin_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/spin_image.h>
#include <pcl/correspondence.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/centroid.h>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/Rotation3DVector.hpp>
#include <rw/math/RPY.hpp>
#include <math.h>       /* acos */
#include <algorithm>    // std::sort
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define PI 3.14159265


bool DEBUG = false;
int SPIN_TIMER = 0;

struct Grasp {
    int AOCscore;
    double robotRelativeScore;
    int environmentScore;
    rw::math::Transform3D<> trans;
    Eigen::Vector4f surfaceNormal;
    double finalScore;
} theGrasp, emptyGrasp;


//Main part of transform function has been made from link:
//http://pointclouds.org/documentation/tutorials/matrix_transform.php
pcl::PointCloud<pcl::PointXYZ>::Ptr transform(pcl::PointCloud<pcl::PointXYZ>::Ptr original_cloud, double x ,double  y, double z, double rotatez, double rotatey, double rotatex){
    Eigen::Affine3f transform = Eigen::Affine3f::Identity();
    transform.translation() << x, y, z;
    transform.rotate (Eigen::AngleAxisf (rotatez, Eigen::Vector3f::UnitZ()));
    transform.rotate (Eigen::AngleAxisf (rotatey, Eigen::Vector3f::UnitY()));
    transform.rotate (Eigen::AngleAxisf (rotatex, Eigen::Vector3f::UnitX()));
    if(DEBUG){
        // Print the transformation
        printf ("\nMethod #2: using an Affine3f\n");
        std::cout << transform.matrix() << std::endl;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::transformPointCloud (*original_cloud, *transformed_cloud, transform);
    return transformed_cloud;
}

//Example from http://pointclouds.org/documentation/tutorials/normal_estimation.php
void makeSurfaceNormal(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::Normal>::Ptr &output){

    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud (cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm (0.03)
    //ne.setRadiusSearch (0.01);
    ne.setKSearch(10);

    // Compute the features
    ne.compute (*cloud_normals);
    //cout << "size of cloud: " << cloud->points.size() << endl;
    //cout << "Size of normals: " << cloud_normals->points.size() << endl;
    output = cloud_normals;
}

Eigen::Vector4f findSurfaceNormal(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, pcl::PointCloud<pcl::Normal>::Ptr normals, int index_target){
    if(cloud->points.size() != normals->points.size()){
        std::cout << "Something went wrong" << std::endl;
    }
    Eigen::Vector4f output;
    for(int i = 0; i < cloud->points.size(); i++){
        if(cloud->points[i].x == cloud->points[index_target].x && cloud->points[i].y == cloud->points[index_target].y && cloud->points[i].z == cloud->points[index_target].z){
            //cout << "TESTTTT: " << i << " " << index_target << endl;
            output[0] = normals->points[i].normal_x;
            output[1] = normals->points[i].normal_y;
            output[2] = normals->points[i].normal_z;
            output[3] = normals->points[i].curvature;
            std::cout << "Found surface normal (" << output[0] << ", " << output[1] << ", " << output[2] << ")" << std::endl;
            return output;

        }
    }
    std::cout << "ERROR: Did not find any surface normal.." << std::endl;
}

bool isCollision(pcl::PointCloud<pcl::PointXYZ>::Ptr target, pcl::PointCloud<pcl::PointXYZ>::Ptr gripperOrFinger1, pcl::PointCloud<pcl::PointXYZ>::Ptr finger2 = nullptr){
    if(finger2 == nullptr){ //Check if manipulator is in collision with target
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
        kdtree.setInputCloud (gripperOrFinger1); //In this case it is gripper
        pcl::PointXYZ searchPoint;
        std::vector<int> pointIdxRadiusSearch;
        std::vector<float> pointRadiusSquaredDistance;
        float radius = 0.005;
        for(int i = 0; i < target->points.size(); i++){
            searchPoint.x = target->points[i].x;
            searchPoint.y = target->points[i].y;
            searchPoint.z = target->points[i].z;

            if ( kdtree.radiusSearch (searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0 ) {
                return true;
            }
        }
        return false;
    }
    else{ //Check if fingers are in collision with target
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree1; // CHECK FIRST FINGER 1, then later FINGER 2
        kdtree1.setInputCloud (gripperOrFinger1); //In this case it is finger1
        pcl::PointXYZ searchPoint1;
        std::vector<int> pointIdxRadiusSearch1;
        std::vector<float> pointRadiusSquaredDistance1;
        float radius = 0.005;
        for(int i = 0; i < target->points.size(); i++){
            searchPoint1.x = target->points[i].x;
            searchPoint1.y = target->points[i].y;
            searchPoint1.z = target->points[i].z;

            if ( kdtree1.radiusSearch (searchPoint1, radius, pointIdxRadiusSearch1, pointRadiusSquaredDistance1) > 0 ) {
                return true;
            }
        }
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree2; // Check finger 2
        kdtree2.setInputCloud (finger2);
        pcl::PointXYZ searchPoint2;
        std::vector<int> pointIdxRadiusSearch2;
        std::vector<float> pointRadiusSquaredDistance2;
        for(int i = 0; i < target->points.size(); i++){
            searchPoint2.x = target->points[i].x;
            searchPoint2.y = target->points[i].y;
            searchPoint2.z = target->points[i].z;

            if ( kdtree2.radiusSearch (searchPoint2, radius, pointIdxRadiusSearch2, pointRadiusSquaredDistance2) > 0 ) {
                return true;
            }
        }
        return false;
    }
}

double collisionDistance(pcl::visualization::PCLVisualizer &viewer, pcl::PointCloud<pcl::PointXYZ>::Ptr target, pcl::PointCloud<pcl::PointXYZ>::Ptr gripperOrFinger1, pcl::PointCloud<pcl::PointXYZ>::Ptr finger2, std::vector<double> &evaluationDistance){
    bool finger1Collision = false;
    bool finger2Collision = false;
    double distance1 = 0;
    double distance2 = 0;
    float step_size_fingers = 0.05;
    int moves = 0;

    while(finger1Collision == false || finger2Collision == false) {
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree1; // CHECK FIRST FINGER 1, then later FINGER 2
        kdtree1.setInputCloud(gripperOrFinger1); //In this case it is finger1
        pcl::PointXYZ searchPoint1;
        std::vector<int> pointIdxRadiusSearch1;
        std::vector<float> pointRadiusSquaredDistance1;
        float radius = 0.005;
        for (int i = 0; i < target->points.size(); i++) {
            searchPoint1.x = target->points[i].x;
            searchPoint1.y = target->points[i].y;
            searchPoint1.z = target->points[i].z;

            if (kdtree1.radiusSearch(searchPoint1, radius, pointIdxRadiusSearch1, pointRadiusSquaredDistance1) >
                0) {
                finger1Collision = true;
            }
        }
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree2; // Check finger 2
        kdtree2.setInputCloud(finger2);
        pcl::PointXYZ searchPoint2;
        std::vector<int> pointIdxRadiusSearch2;
        std::vector<float> pointRadiusSquaredDistance2;
        for (int i = 0; i < target->points.size(); i++) {
            searchPoint2.x = target->points[i].x;
            searchPoint2.y = target->points[i].y;
            searchPoint2.z = target->points[i].z;

            if (kdtree2.radiusSearch(searchPoint2, radius, pointIdxRadiusSearch2, pointRadiusSquaredDistance2) >
                0) {
                finger2Collision = true;
            }
        }
        //Step here:
        if (finger2Collision == false) {
            finger2 = transform(finger2, -step_size_fingers, 0, 0, 0, 0, 0);
            distance2 += step_size_fingers;
            cout << "move finger2" << endl;
            moves++;
            viewer.updatePointCloud(gripperOrFinger1, "Finger1");
            viewer.updatePointCloud(finger2, "Finger2");
            viewer.spinOnce(SPIN_TIMER);

        }
        if (finger1Collision == false) {
            gripperOrFinger1 = transform(gripperOrFinger1, step_size_fingers, 0, 0, 0, 0, 0);
            distance1 += step_size_fingers;
            cout << "move finger1" << endl;
            moves++;
            viewer.updatePointCloud(gripperOrFinger1, "Finger1");
            viewer.updatePointCloud(finger2, "Finger2");
            viewer.spinOnce(SPIN_TIMER);
        }
        if(moves > 100){
            break;
        }
    }
    if (finger1Collision == true && finger2Collision == true) {
        evaluationDistance.push_back(distance1 + distance2);
        return distance1 + distance2;
    }
    cout << "ERROR: DISTANCE CALC" << endl;
    return -10000;
}


bool contains(std::vector<int> v, int target){
    if(std::find(v.begin(), v.end(), target) != v.end()) {
        return true;
    } else {
        return false;
    }
}


pcl::PointXYZ findTCP(pcl::PointCloud<pcl::PointXYZ>::Ptr PG70){
    //Find highest point
    double y = PG70->points[0].y;
    for(int i = 0; i < PG70->points.size(); i++){
        if(PG70->points[i].y > y){
            y = PG70->points[i].y;
        }
    }
    //cout << "y: " << y << endl;
    double z = 0;
    double x = 0;
    //Take average of x and z:
    for(int i = 0; i < PG70->points.size(); i++){
        z += PG70->points[i].z;
        x += PG70->points[i].x;
    }
    z /= PG70->points.size();
    x /= PG70->points.size();

    pcl::PointXYZ tcp;
    tcp.x = x;
    tcp.y = y;
    tcp.z = z;
    return tcp;

    //cout << "ERROR: did not find any TCP point" << endl;
}

int evaluateGrasp(pcl::PointCloud<pcl::PointXYZ>::Ptr target, pcl::PointCloud<pcl::PointXYZ>::Ptr finger1, pcl::PointCloud<pcl::PointXYZ>::Ptr finger2, std::vector<int> &idxTargetTouchedEvaluation){
    int areaOfContact = 0;
    float radius = 0.005;


    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree1; //Check finger 1
    kdtree1.setInputCloud (finger1);
    pcl::PointXYZ searchPoint1;
    std::vector<int> pointIdxRadiusSearch1;
    std::vector<float> pointRadiusSquaredDistance1;
    for(int i = 0; i < target->points.size(); i++){
        searchPoint1.x = target->points[i].x;
        searchPoint1.y = target->points[i].y;
        searchPoint1.z = target->points[i].z;

        if ( kdtree1.radiusSearch (searchPoint1, radius, pointIdxRadiusSearch1, pointRadiusSquaredDistance1) > 0 ) {
            areaOfContact++;
            if(contains(idxTargetTouchedEvaluation, i) == false){
                idxTargetTouchedEvaluation.push_back(i);
            }
        }
    }
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree2; // Check finger 2
    kdtree2.setInputCloud (finger2);
    pcl::PointXYZ searchPoint2;
    std::vector<int> pointIdxRadiusSearch2;
    std::vector<float> pointRadiusSquaredDistance2;
    for(int i = 0; i < target->points.size(); i++){
        searchPoint2.x = target->points[i].x;
        searchPoint2.y = target->points[i].y;
        searchPoint2.z = target->points[i].z;

        if ( kdtree2.radiusSearch (searchPoint2, radius, pointIdxRadiusSearch2, pointRadiusSquaredDistance2) > 0 ) {
            areaOfContact++;
            if(contains(idxTargetTouchedEvaluation, i) == false){
                idxTargetTouchedEvaluation.push_back(i);
            }
        }
    }
    return areaOfContact;
}

void multiplyTransforms(rw::math::Transform3D<> &transform, double x, double y, double z, double rotatez, double rotatey, double rotatex){
    rw::math::Vector3D<> trans(x,y,z);
    rw::math::RPY<> rot(rotatez,rotatey,rotatex);
    rw::math::Transform3D<> t(trans, rot.toRotation3D());
    transform = transform*t;
    cout << "Keeping track of pose (TRANSFORMATION): " << endl;
    cout << transform << endl;
}

void addPosTransform(rw::math::Transform3D<> &transform, double dX, double dY, double dZ){
    double x = transform.e()(0,3);
    double y = transform.e()(1,3);
    double z = transform.e()(2,3);
    rw::math::Vector3D<> trans(x+dZ, y+dY, z+dZ);
    rw::math::Transform3D<> t(trans, transform.R());
    transform = t;
    cout << "Keeping track of pose (TRANSFORMATION): " << endl;
    cout << transform << endl;
}
double robotRelativePositionScore(Grasp grasp, Eigen::Vector3f robotToTargetVec){
    double dotProduct = grasp.trans.P()[0] * robotToTargetVec[0] + grasp.trans.P()[1] * robotToTargetVec[1] + grasp.trans.P()[2] * robotToTargetVec[2];
    double lengthManipulatorVec = sqrt(grasp.trans.P()[0]*grasp.trans.P()[0] + grasp.trans.P()[1]*grasp.trans.P()[1]+grasp.trans.P()[2]*grasp.trans.P()[2]);
    double lengthRobotVec = sqrt(robotToTargetVec[0]*robotToTargetVec[0]+robotToTargetVec[1]*robotToTargetVec[1]+robotToTargetVec[2*robotToTargetVec[2]]);
    double normalize = normalize;
    double angle = acos(dotProduct/normalize);
    return 180 - angle; //If angle is low, give good score
}

int environmentScore(pcl::PointCloud<pcl::PointXYZ>::Ptr PG70, pcl::PointCloud<pcl::PointXYZ>::Ptr finger1, pcl::PointCloud<pcl::PointXYZ>::Ptr finger2, pcl::PointCloud<pcl::PointXYZ>::Ptr scene){
    //Check once if manipulator is collision free
    //(only object is table, so if it's free at the grasp it's likely to be collision free)
    //Because the environment is static, this can be computed offline
    int collisions = 0;
    float radius = 0.005;

    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree1; //Check finger 1
    kdtree1.setInputCloud (finger1);
    pcl::PointXYZ searchPoint1;
    std::vector<int> pointIdxRadiusSearch1;
    std::vector<float> pointRadiusSquaredDistance1;
    for(int i = 0; i < scene->points.size(); i++){
        searchPoint1.x = scene->points[i].x;
        searchPoint1.y = scene->points[i].y;
        searchPoint1.z = scene->points[i].z;

        if ( kdtree1.radiusSearch (searchPoint1, radius, pointIdxRadiusSearch1, pointRadiusSquaredDistance1) > 0 ) {
            collisions++;
        }
    }

    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree2; //Check finger 2
    kdtree2.setInputCloud (finger2);
    pcl::PointXYZ searchPoint2;
    std::vector<int> pointIdxRadiusSearch2;
    std::vector<float> pointRadiusSquaredDistance2;
    for(int i = 0; i < scene->points.size(); i++){
        searchPoint1.x = scene->points[i].x;
        searchPoint1.y = scene->points[i].y;
        searchPoint1.z = scene->points[i].z;

        if ( kdtree2.radiusSearch (searchPoint2, radius, pointIdxRadiusSearch2, pointRadiusSquaredDistance2) > 0 ) {
            collisions++;
        }
    }

    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree3; //Check PG70
    kdtree3.setInputCloud (PG70);
    pcl::PointXYZ searchPoint3;
    std::vector<int> pointIdxRadiusSearch3;
    std::vector<float> pointRadiusSquaredDistance3;
    for(int i = 0; i < scene->points.size(); i++){
        searchPoint1.x = scene->points[i].x;
        searchPoint1.y = scene->points[i].y;
        searchPoint1.z = scene->points[i].z;

        if ( kdtree3.radiusSearch (searchPoint3, radius, pointIdxRadiusSearch3, pointRadiusSquaredDistance3) > 0 ) {
            collisions++;
        }
    }
    return collisions;
}



Grasp graspPlanning(pcl::visualization::PCLVisualizer &viewer, int index_target, double p_rotation, pcl::PointCloud<pcl::PointXYZ>::Ptr target, pcl::PointCloud<pcl::PointXYZ>::Ptr finger1, pcl::PointCloud<pcl::PointXYZ>::Ptr finger2, pcl::PointCloud<pcl::PointXYZ>::Ptr PG70, pcl::PointCloud<pcl::PointXYZ>::Ptr scene, std::vector<int> &evaluation, std::vector<double> &evaluationDistance){
    cout << "index target: " << index_target << endl;

    cout << "Adding pointclouds.." << endl;
    //Add pointclouds
    viewer.addPointCloud (PG70,"Manipulator");
    viewer.addPointCloud (finger1,"Finger1");
    viewer.addPointCloud (finger2,"Finger2");
    viewer.addPointCloud (target,"Target");
    //viewer.addCoordinateSystem(1, 0);

    viewer.spinOnce(SPIN_TIMER);

    cout << "Assembling pointclouds.." << endl;
    //Assembly manipulator and fingers
    Eigen::Vector4f centroidObj;
    pcl::compute3DCentroid (*finger1, centroidObj);
    //cout << "centroid object:" << centroidObj[0] << " " << centroidObj[1] << " " << centroidObj[2] << " " << centroidObj[3] << " \n";

    Eigen::Vector4f centroidObj1;
    pcl::compute3DCentroid (*finger2, centroidObj1);
    //cout << "centroid object:" << centroidObj1[0] << " " << centroidObj1[1] << " " << centroidObj1[2] << " " << centroidObj1[3] << " \n";

    Eigen::Vector4f centroidManipulator;
    pcl::compute3DCentroid (*PG70, centroidManipulator);
    //cout << "centroid manipulator:" << centroidManipulator[0] << " " << centroidManipulator[1] << " " << centroidManipulator[2] << " " << centroidManipulator[3] << " \n";

    //cout << "Calculating centroids" << endl;

    Eigen::Vector4f centroidDiff;
    centroidDiff[0] = centroidManipulator[0] - centroidObj[0];
    centroidDiff[1] = centroidManipulator[1] - centroidObj[1];
    centroidDiff[2] = centroidManipulator[2] - centroidObj[2];

    Eigen::Vector4f centroidDiff1;
    centroidDiff1[0] = centroidManipulator[0] - centroidObj1[0];
    centroidDiff1[1] = centroidManipulator[1] - centroidObj1[1];
    centroidDiff1[2] = centroidManipulator[2] - centroidObj1[2];

    finger1 = transform(finger1, centroidDiff[0], centroidDiff[1], centroidDiff[2], 0, 0, 0);
    finger1 = transform(finger1,0.2,4.9,0, 0, (3.14/2), -(3.14/2));
    viewer.updatePointCloud(finger1, "Finger1");

    finger2 = transform(finger2, centroidDiff1[0], centroidDiff1[1], centroidDiff1[2],0,0,0);
    finger2 = transform(finger2,-0.2,4.9,0,0,-(3.14/2), -(3.14/2));
    viewer.updatePointCloud(finger2, "Finger2");
    viewer.spinOnce(SPIN_TIMER);

    //For pose calculation in the end:
    //First place target and manipulator so the translation and rotation is 0.
    //Then keep track of transformation of target and return it in the end.
    rw::math::Vector3D<> translation(0,0,0);
    rw::math::RPY<> rotation(0,0,0);
    rw::math::Transform3D<> transformOfTarget(translation, rotation.toRotation3D());
    //Return this if no grasp can be found
    rw::math::Transform3D<> empty(translation, rotation.toRotation3D());
    emptyGrasp.trans = empty;
    emptyGrasp.AOCscore = 0;

    cout << "Finding tcp.. " << endl;
    //Find TCP of manipulator
    pcl::PointXYZ tcp = findTCP(PG70);
    //pcl::PointXYZ tcp;
    //tcp.x = centroidManipulator[0];
    //tcp.y = centroidManipulator[1];
    //tcp.z = centroidManipulator[2];

    viewer.spin();

    cout << "Placing target at the manipulator so the translation and rotation is 0 (For pose calculation in the end" << endl;
    Eigen::Vector4f centroidTarget;
    Eigen::Vector3f centroidDiffTarget;
    pcl::compute3DCentroid (*target, centroidTarget);
    centroidDiffTarget[0] = tcp.x - centroidTarget[0];
    centroidDiffTarget[1] = tcp.y - centroidTarget[1];
    centroidDiffTarget[2] = tcp.z - centroidTarget[2];

    target = transform(target,centroidDiffTarget[0] ,centroidDiffTarget[1] ,centroidDiffTarget[2] , 0, 0, 0); //Manipulator and target now has same pose, save all transformation of the target
    viewer.updatePointCloud(target, "Target");
    viewer.spinOnce(SPIN_TIMER);
    viewer.spin();

    float dX = tcp.x - target.get()->points[index_target].x;
    float dY = tcp.y - target.get()->points[index_target].y;
    float dZ = tcp.z - target.get()->points[index_target].z;

    cout << "dX: " << dX << endl;
    cout << "dY: " << dY << endl;
    cout << "dZ: " << dZ << endl;

    target = transform(target, dX, dY, dZ, 0, 0, 0); // Move target point to tcp
    multiplyTransforms(transformOfTarget,  dX, dY, dZ, 0, 0, 0);
    viewer.updatePointCloud(target, "Target");
    viewer.spin();
    viewer.spinOnce(SPIN_TIMER);

    cout << "Initialize surface normals.." << endl;
    //Init surface normals
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    makeSurfaceNormal(target, cloud_normals);

    //Displays normal surfaces
    //viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(target, cloud_normals,1, 0.02, "normals");
    //viewer.updatePointCloud(target, "Target");
    //viewer.spin();

    cout << "Finding surface normal for p_target.." << endl;
    //Find normal surface of p_target
    Eigen::Vector4f surfaceNormalVector;
    surfaceNormalVector = findSurfaceNormal(target, cloud_normals, index_target);
    cout << "surfaceNormalVector[0]: " << surfaceNormalVector[0] << endl;
    cout << "surfaceNormalVector[1]: " << surfaceNormalVector[1] << endl;
    cout << "surfaceNormalVector[2]: " << surfaceNormalVector[2] << endl;
    theGrasp.surfaceNormal = surfaceNormalVector;

    double anglex = acos(surfaceNormalVector[0]);
    double angley = acos(surfaceNormalVector[1]);
    double anglez = acos(surfaceNormalVector[2]);

    cout << "anglex: " << anglex*180/PI << endl;
    cout << "angley: " << angley*180/PI << endl;
    cout << "anglez: " << anglez*180/PI << endl;

    cout << "Placing p_target at TCP and rotate to match P_d.." << endl;
    //place p_target on p_target at TCP and Rotate object to match P_d *AVOID transforming manipulator and two fingers*

    //Move back to center to rotate
    target = transform(target, -dX-centroidDiffTarget[0], -dY-centroidDiffTarget[1], -dZ-centroidDiffTarget[2], 0, 0, 0);
    viewer.updatePointCloud(target, "Target");
    viewer.spin();


    cout << "rotate" << endl;
    target = transform(target, 0, 0, 0, 0,anglex+p_rotation,angley);
    multiplyTransforms(transformOfTarget,  0, 0, 0,0,anglex+p_rotation,angley);
    viewer.updatePointCloud(target, "Target");
    viewer.spin();

    cout << "move back" << endl;
    target = transform(target, dX+centroidDiffTarget[0], dY+centroidDiffTarget[1], dZ+centroidDiffTarget[2], 0, 0, 0); //Move back to to position before rotation
    viewer.updatePointCloud(target, "Target");
    viewer.spin();
    viewer.spinOnce(SPIN_TIMER);

/*
    target = transform(target, 0, 0, 0, 0,p_rotation,0);
    multiplyTransforms(transformOfTarget,  0, 0, 0,0,p_rotation,0);
    viewer.updatePointCloud(target, "Target");
    viewer.spinOnce(SPIN_TIMER);
    viewer.spin();
*/


    dX = tcp.x - target.get()->points[index_target].x;
    dY = tcp.y - target.get()->points[index_target].y;
    dZ = tcp.z - target.get()->points[index_target].z;

    cout << "dX: " << dX << endl;
    cout << "dY: " << dY << endl;
    cout << "dZ: " << dZ << endl;

    target = transform(target, dX, dY, dZ, 0, 0, 0);
    //Add manually here, since we dont want to translate in the new transformed coordinate system, but the original.
    addPosTransform(transformOfTarget, dX, dY, dZ);

    viewer.updatePointCloud(target, "Target");
    viewer.spin();
    viewer.spinOnce(SPIN_TIMER);

    cout << "Moving object until no collision with manipulator.." << endl;
    //Move manipulator -P_d until no collision
    bool collision = true;
    float step_size = 0.3; //Try different step_size
    while(collision){ //check until no collision
        //cout << "Moving target further away.." << endl;
        target = transform(target, 0,step_size,0,0,0,0);
        //multiplyTransforms(transformOfTarget, 0, step_size, 0, 0, 0, 0);
        addPosTransform(transformOfTarget, 0, step_size, 0);
        //Visualize
        viewer.updatePointCloud(target, "Target");
        viewer.spinOnce(SPIN_TIMER);
        viewer.spin();

        collision = isCollision(target, PG70);
    }
    cout << "Gripper no longer in collision" << endl;

    viewer.spinOnce(SPIN_TIMER);

    //Wrap fingers until collision ###FIRST CHECK SHOULD BE COLLISION FREE
    collision = isCollision(target, finger1, finger2);
    if(collision){ //First check must not be in collision
        cout << "First finger check is in collision, discarding grasp" << endl;
        viewer.removePointCloud("Manipulator");
        viewer.removePointCloud("Finger1");
        viewer.removePointCloud("Finger2");
        viewer.removePointCloud("Target");
        return emptyGrasp; //Discard grasp
    }
    cout << "First finger check is not in collision" << endl;
    bool fingerLimit = false;
    float step_size_fingers = 0.05;
    int iteration = 0;
    while(true) {
        if (collision == false) { //check until collision
            //cout << "Closing fingers" << endl;
            finger1 = transform(finger1, step_size_fingers, 0, 0, 0, 0, 0);
            finger2 = transform(finger2, -step_size_fingers, 0, 0, 0, 0, 0);

            //Visualize
            viewer.updatePointCloud(finger1, "Finger1");
            viewer.updatePointCloud(finger2, "Finger2");
            viewer.spinOnce(SPIN_TIMER);

            collision = isCollision(target, finger1, finger2);
            if(iteration == 15){ //Fingers can not move any closer
                cout << "Fingers can not move any closer, discarding grasp" << endl;
                viewer.removePointCloud("Manipulator");
                viewer.removePointCloud("Finger1");
                viewer.removePointCloud("Finger2");
                viewer.removePointCloud("Target");
                return emptyGrasp; //Discard grasp
            }
            iteration++;
        }
        else{
            collisionDistance(viewer, target, finger1, finger2, evaluationDistance);
            cout << "Breaking, atleast one finger in collision" << endl;
            break; //stop while
        }
    }

    //viewer.spinOnce(SPIN_TIMER);
    int score = evaluateGrasp(target, finger1, finger2, evaluation);
    theGrasp.AOCscore = score;
    cout << "Evaluating score: " << score << endl;
    cout << "Calculating pose of the gripper: " << endl;
    rw::math::Transform3D<> transformOfGripper = rw::math::inverse(transformOfTarget); //Go FROM manipulator to target TO target to manipulator
    cout << "transform gripper to target: " << transformOfTarget << endl;
    cout << "transform target to gripper: " << transformOfGripper << endl;
    theGrasp.trans = transformOfGripper;

    cout << "Done.." << endl;
    viewer.spinOnce(SPIN_TIMER);
    viewer.spin();

    //Clean up viewer:
    viewer.removePointCloud("Manipulator");
    viewer.removePointCloud("Finger1");
    viewer.removePointCloud("Finger2");
    viewer.removePointCloud("Target");

    return theGrasp;

}



std::vector<Grasp> calcrobRelPosScoreAndOrder(std::vector<Grasp> graspSet, Eigen::Matrix4f pose){
    double c1 = 5; //coeffs (weights)
    double c2 = 1;
    std::vector<Grasp> orderedGrasps;
    cout << "Calculate RobotRelativePositionScore and order" << endl;
    for(int i = 0; i < graspSet.size(); i++){
        //double robRelPosScore = robotRelativePositionScore(graspSet.at(i),pose); //TODO calc
        double robRelPosScore = 0;
        graspSet.at(i).finalScore = c1*graspSet.at(i).AOCscore - c2*robRelPosScore;
        if(orderedGrasps.size() == 0){
            orderedGrasps.push_back(graspSet.at(0));
        }else {
            bool insert = false;
            for(int j = 0; j < orderedGrasps.size(); j++) { //Sort
                if (graspSet.at(i).finalScore > orderedGrasps.at(j).finalScore) {
                    orderedGrasps.insert(orderedGrasps.begin() + j, graspSet.at(i));
                    insert = true;
                    break;
                }
            }
            if(insert == false){
                orderedGrasps.push_back(graspSet.at(i));
            }
        }
    }
    //COUT
    for(int i = 0; i < orderedGrasps.size(); i++ ){
        cout << "Score: " << orderedGrasps.at(i).finalScore << endl;
    }

    return orderedGrasps;
}



int main(int argc, char** argv){

    pcl::PointCloud<pcl::PointXYZ>::Ptr finger1 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr finger2 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr manipulator (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr target (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr scene (new pcl::PointCloud<pcl::PointXYZ>);


    if (pcl::io::loadPCDFile<pcl::PointXYZ> (argv[1], *finger2) == -1 ||
        pcl::io::loadPCDFile<pcl::PointXYZ> (argv[2], *finger1) == -1 ||
        pcl::io::loadPCDFile<pcl::PointXYZ> (argv[3], *manipulator) == -1 ||
        pcl::io::loadPCDFile<pcl::PointXYZ> (argv[4], *target) == -1 ||
        pcl::io::loadPCDFile<pcl::PointXYZ> (argv[5], *scene) == -1)//* load the files
    {
        PCL_ERROR ("Failed to load PointCloud \n" );
    }
    //pcl::PointXYZ p_target;
    pcl::visualization::PCLVisualizer viewer("Cloud Viewer");
    //pcl::visualization::PCLVisualizer evaluationViewer("EvaViewer");


    srand (time(NULL));
    std::vector<int> evaluation;
    int index_target;
    double p_rotation;
    int numberOfGrasps = 10;
    Eigen::Matrix4f pose; //TODO get pose
    std::vector<Grasp> graspSet;
    std::vector<double> evaluationDistance;
    for(int i = 0; i < numberOfGrasps; i++){
        cout << "Grasp i: " << i << " out of: " << numberOfGrasps << endl;
        index_target = rand() % target->points.size();
        p_rotation = rand() % 180;
        graspSet.push_back(graspPlanning(viewer, index_target, p_rotation,target, finger1, finger2, manipulator, scene, evaluation, evaluationDistance));
    }
    graspSet = calcrobRelPosScoreAndOrder(graspSet, pose);
    cout << "exit main.." << endl;

    //Evaluation
    cout << "Evaluation" << endl;
    for(int i = 0; i < evaluation.size(); i++){
        cout << "i: " << evaluation.at(i) << endl;
    }

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_rgb (new pcl::PointCloud<pcl::PointXYZRGB>);
    cloud_rgb->resize(target->size());
    for (size_t i = 0; i < target->points.size(); i++) {
        cloud_rgb->points[i].x = target->points[i].x;
        cloud_rgb->points[i].y = target->points[i].y;
        cloud_rgb->points[i].z = target->points[i].z;
        cloud_rgb->points[i].r = 255;
        cloud_rgb->points[i].g = 0;
        cloud_rgb->points[i].b = 0;
        for(int j = 0; j < evaluation.size(); j++){ //If this point has been touched, set it green
            if(i == evaluation[j]){
                //cout << "Green" << endl;
                cloud_rgb->points[i].r = 0;
                cloud_rgb->points[i].g = 255;
            }
        }
    }
    //evaluationViewer.addPointCloud (cloud_rgb, "rbg_target");
    viewer.removePointCloud("Manipulator");
    viewer.removePointCloud("Finger1");
    viewer.removePointCloud("Finger2");
    viewer.removePointCloud("Target");
    viewer.removeCoordinateSystem();


    cout << "Removing everything and add evaluation" << endl;
    viewer.addPointCloud (cloud_rgb, "rgb");
    viewer.spin();
    for (size_t i = 0; i < cloud_rgb->points.size(); i++) {
        if(cloud_rgb->points[i].r == 255){
            cloud_rgb->points[i].r = 0;
        }
    }
    viewer.updatePointCloud(cloud_rgb, "rgb");
    viewer.spin();

    cout << "Evaluation of distance: " << endl;
    for(int i = 0; i < evaluationDistance.size(); i++){
        cout << "Distance: " << evaluationDistance.at(i) << endl;
    }

    ofstream distance;
    distance.open ("evaDISTANCE.txt");
    for(int i = 0; i < evaluationDistance.size(); i++){
        cout << "Saving distance.." << endl;
        distance << evaluationDistance.at(i);
        distance << "\n";
    }
    distance.close();

    ofstream outputFile;
    outputFile.open ("poses.txt");
    for(int i = 0; i < graspSet.size(); i++){
        if(graspSet[i].AOCscore > 0){
            cout << "Saving grasp.." << endl;
            //If score larger than 0, then save to file
            outputFile << graspSet[i].trans;
            //outputFile << graspSet[i].trans.P().x << " " << graspSet[i].trans.P().y << " " << graspSet[i].trans.P().z << " ";
            //outputFile << graspSet[i].trans.R().m().at_element(0,0) << " " << graspSet[i].trans.R().m().at_element(0,1) << " " << graspSet[i].trans.R().m().at_element(0,2) << " " << graspSet[i].trans.R().m().at_element(1,0) << " " << graspSet[i].trans.R().m().at_element(1,1) << " " << graspSet[i].trans.R().m().at_element(1,2) << " " << graspSet[i].trans.R().m().at_element(2,0) << " " << graspSet[i].trans.R().m().at_element(2,1) << " " << graspSet[i].trans.R().m().at_element(2,2);
            outputFile << "\n";
        }
    }
    outputFile.close();
    ;
    return 0;
/*
    pcl::visualization::PCLVisualizer viewer("Cloud Viewer");
    viewer.addPointCloud (manipulator,"manipulator");
    viewer.addPointCloud (finger1,"finger");
    viewer.addPointCloud (finger2, "finger1");


    Eigen::Vector4f centroidObj;
    pcl::compute3DCentroid (*finger1, centroidObj);
    cout << "centroid object:" << centroidObj[0] << " " << centroidObj[1] << " " << centroidObj[2] << " " << centroidObj[3] << " \n";

    Eigen::Vector4f centroidObj1;
    pcl::compute3DCentroid (*finger2, centroidObj1);
    cout << "centroid object:" << centroidObj1[0] << " " << centroidObj1[1] << " " << centroidObj1[2] << " " << centroidObj1[3] << " \n";

    Eigen::Vector4f centroidManipulator;
    pcl::compute3DCentroid (*manipulator, centroidManipulator);
    cout << "centroid manipulator:" << centroidManipulator[0] << " " << centroidManipulator[1] << " " << centroidManipulator[2] << " " << centroidManipulator[3] << " \n";

    Eigen::Vector4f centroidDiff;
    centroidDiff[0] = centroidManipulator[0] - centroidObj[0];
    centroidDiff[1] = centroidManipulator[1] - centroidObj[1];
    centroidDiff[2] = centroidManipulator[2] - centroidObj[2];

    Eigen::Vector4f centroidDiff1;
    centroidDiff1[0] = centroidManipulator[0] - centroidObj1[0];
    centroidDiff1[1] = centroidManipulator[1] - centroidObj1[1];
    centroidDiff1[2] = centroidManipulator[2] - centroidObj1[2];


    finger1 = transform(finger1, centroidDiff[0], centroidDiff[1], centroidDiff[2], 0, 0, 0);
    //object = transform(object,-0.85,0.60,5.07, (3.14/2), (3.14/2), 0);
    finger1 = transform(finger1,0.2,4.9,0, 0, -(3.14/2), (3.14/2));
    viewer.updatePointCloud(finger1, "finger");

    finger2 = transform(finger2, centroidDiff1[0], centroidDiff1[1], centroidDiff1[2],0,0,0);
    finger2 = transform(finger2,-0.2,4.9,0,0,-(3.14/2), -(3.14/2));
    viewer.updatePointCloud(finger2, "finger1");
    */

    return 0;



}