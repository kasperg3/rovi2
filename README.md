# README

## Realsense RosNode
__Launch Realsense RosNode:__ 
```
roslaunch realsense2_camera rs_rgbd.launch
```

#### with filter and clip distance:
```
roslaunch realsense2_camera rs_rgbd.launch filters:=temporal clip_distance:=1.6
```
#### or for accessing Right/left images 
```
roslaunch realsense2_camera  rs_camera.launch
```

## Connect to UR: 
__1: Specify the ip in the robots interface.__ Settings -> Network -> IP:192.168.100.1

__2: Make your wired connection static and set ip to__ 192.168.100.2

__3: Establish a connection between the robot and the node:__
```
roslaunch caros_universalrobot caros_universalrobot.launch device_ip:=192.168.100.1
```

__4: run your program, eg:__
```
roslaunch caros_universalrobot simple_demo_using_move_ptp.test
```

## Run a program on ursim: 
__1: Launch the URSim and start the sim:__
```
    sudo /home/USER/ursim-5.2.0.61336/start-ursim.sh UR5
``` 
__2: Run the program eg.:__ 
```
    rosrun rovi2 URRobot
```

## Gripper

__1: Onetime setup:__
```
    sudo adduser your-username dialout
```
__2: Restart for this to take effect!__

__3: Launch Ros node__
```
    roslaunch caros_schunkpg70 caros_schunkpg70.launch
```

__Useful API Docs:__

https://caro-sdu.gitlab.io/caros/caros_control/html/c++/classcaros_1_1GripperSIProxy.html

https://caro-sdu.gitlab.io/caros/caros_control/html/c++/index.html

https://caro-sdu.gitlab.io/caros/caros_control/html/c++/classcaros_1_1GripperServiceInterface.html#details



## Camera Calibration

http://wiki.ros.org/camera_calibration/Tutorials/StereoCalibration

## Creating a new ROS-Node

https://husarion.com/tutorials/ros-tutorials/2-creating-nodes/


## Camera Subscriber

__1: Run the cameranode to make the topics available:__
```
    roslaunch realsense2_camera rs_rgbd.launch 
```

__2:Run the CameraSubscriber node:__ 
```
    rosrun rovi2 camerasubscriber
```
## ROSBAG Play
__1: Play rosbag on repeat:__
```
    rosbag play -l bag_name.bag
```

## ROSBAG Record

__1: Record__
```
    rosbag record -e "(.*)sensors(.*)"
```
__2: Exit__
```
    Ctrl + c
```

__Relevant links used__
Subscriber for acquiring stereo images(L/R images):

http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29

http://wiki.ros.org/cv_bridge/Tutorials/UsingCvBridgeToConvertBetweenROSImagesAndOpenCVImages

Recording camera bag file to playback:

http://wiki.ros.org/ROS/Tutorials/Recording%20and%20playing%20back%20data

For synchronizing multiple subscriber topics: 

http://wiki.ros.org/message_filters#Example_.28Python.29-1

https://answers.ros.org/question/280856/synchronizer-with-approximate-time-policy-in-a-class-c/
    
## Object detection

http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29

http://www.pointclouds.org/assets/iros2011/features.pdf


## Camera calibration
__To run calibration:__
```
    rosrun camera_calibration cameracalibrator.py --approximate 0.1 --size 9x6 --square 0.04 right:=/camera/infra1/image_rect_raw left:=/camera/infra2/image_rect_raw right_camera:=/camera/infra1 left_camera:=/camera/infra2 --no-service-check
```
Where __size__ is the size of the checkerboard, __square__ is the size of the checkers, __approximate__ is set to ensure run if timestamps of the two cameras are not exactly the same. 

## AR Tracker Install
__1:Clone into Catkin dir__
```
    cd ~/catkin_ws/ &&
    git clone https://github.com/ros-perception/ar_track_alvar.git
```

__2:Install with apt-get__
```
    sudo apt-get install ros-melodic-ar-track-alvar
```
__3:Copy launchfile from gitlab to cloned dir__
```
    cp ~/catkin_ws/src/rovi2/CamConfig/ar_test.launch ~/catkin_ws/src/ar_track_alvar/ar_track_alvar/launch/
```

## AR Tracker for hand to cam calibration
__Launch Realsense RosNode__ 
```
roslaunch realsense2_camera rs_rgbd.launch
```
__Launch Caros Universal Robots RosNode__ 
```
roslaunch caros_universalrobot caros_universalrobot.launch device_ip:=192.168.100.1
```
Or use the UR simulator

__Launch rviz__ 
```
rviz
```
__Launch the AR Tracker__ 
```
roslaunch ar_track_alvar ar_test.launch 
```

__Vizualize the tracking in rviz__ 

Change the fixed frame in the drop down menu to camera_color_optical_frame and press "Add". Given, that your marker is in frame of the camera, you should be able to see the marker in rviz. 


